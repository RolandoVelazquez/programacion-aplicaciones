<?php

namespace App\Http\Controllers;

use App\Products;
use App\Stores;
use Illuminate\Http\Request;

class ShowProducts extends Controller
{
    public function getProducts($id)
    {
        $products = Stores::with('products')->find($id);
        //dd($products);
        return view('pages.home', compact("products"));
    }
}
