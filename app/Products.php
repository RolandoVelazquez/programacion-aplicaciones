<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Products extends Model
{
    public function store(){
        return $this->belongsToMany(Stores::class, 'id');
    }
}
