<!DOCTYPE html>
<html lang="en-US">
<head>
    <title>MVC TV Example</title>
    <link rel="stylesheet" href="{{asset('css/bulma.min.css')}}">

</head>
<body>
<style>
    img {
        height: 400px;
        left: 21%;
        position: absolute;
        top: 21%;
        width: 600px;
    }
</style>
<p>Estas viendo: </p>
<p>{{ $ejemplo }}</p>
@if ($channel && count($channel) > 0)
    <img src="{{asset( $channel[0]->image )}}" />
    @else
    <img src="{{asset( 'imgs/nosignal.jpg' )}}" />
@endif

<div class="dropdown is-active">
    <div class="dropdown-trigger">
        <button class="button" aria-haspopup="true" aria-controls="dropdown-menu">
            <span>Dropdown button</span>
            <span class="icon is-small">
        <i class="fas fa-angle-down" aria-hidden="true"></i>
      </span>
        </button>
    </div>
    <div class="dropdown-menu" id="dropdown-menu" role="menu">
        <div class="dropdown-content">
            <a href="#" class="dropdown-item">
                Dropdown item
            </a>
            <a class="dropdown-item">
                Other dropdown item
            </a>
            <a href="#" class="dropdown-item is-active">
                Active dropdown item
            </a>
            <a href="#" class="dropdown-item">
                Other dropdown item
            </a>
            <hr class="dropdown-divider">
            <a href="#" class="dropdown-item">
                With a divider
            </a>
        </div>
    </div>
</div>
</body>
</html>
